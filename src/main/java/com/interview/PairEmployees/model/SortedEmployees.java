package com.interview.PairEmployees.model;

public class SortedEmployees {
	
	private int emp1;
	private int emp2;
	private Long emp1DaysWorkedOnProject;
	private Long emp2DaysWorkedOnProject;
	private Long daysWorkedTogether;
	private int projectBothWorkedOn;
	
	public SortedEmployees() {
		
	}
	
	public SortedEmployees(int emp1, int emp2, Long daysWorkedTogether, int projectBothWorkedOn, Long emp1DaysWorkedOnProject, Long emp2DaysWorkedOnProject) {
		this.emp1 = emp1;
		this.emp2 = emp2;
		this.daysWorkedTogether = daysWorkedTogether;
		this.projectBothWorkedOn = projectBothWorkedOn;
		this.emp1DaysWorkedOnProject = emp1DaysWorkedOnProject;
		this.emp2DaysWorkedOnProject = emp2DaysWorkedOnProject;
	}

	public int getEmp1() {
		return emp1;
	}

	public void setEmp1(int emp1) {
		this.emp1 = emp1;
	}

	public int getEmp2() {
		return emp2;
	}

	public void setEmp2(int emp2) {
		this.emp2 = emp2;
	}

	public Long getDaysWorkedTogether() {
		return daysWorkedTogether;
	}

	public void setDaysWorkedTogether(Long daysWorkedTogether) {
		this.daysWorkedTogether = daysWorkedTogether;
	}

	public int getProjectBothWorkedOn() {
		return projectBothWorkedOn;
	}

	public void setProjectBothWorkedOn(int projectBothWorkedOn) {
		this.projectBothWorkedOn = projectBothWorkedOn;
	}

	public Long getEmp1DaysWorkedOnProject() {
		return emp1DaysWorkedOnProject;
	}

	public void setEmp1DaysWorkedOnProject(Long emp1StartDate) {
		this.emp1DaysWorkedOnProject = emp1StartDate;
	}

	public Long getEmp2DaysWorkedOnProject() {
		return emp2DaysWorkedOnProject;
	}

	public void setEmp2DaysWorkedOnProject(Long emp2StartDate) {
		this.emp2DaysWorkedOnProject = emp2StartDate;
	}

	@Override
	public String toString() {
		return "SortedEmployees [emp1=" + emp1 + ", emp2=" + emp2 + ", emp1StartDate=" + emp1DaysWorkedOnProject
				+ ", emp2StartDate=" + emp2DaysWorkedOnProject + ", daysWorkedTogether=" + daysWorkedTogether + ", projectBothWorkedOn=" + projectBothWorkedOn + "]";
	}

}
