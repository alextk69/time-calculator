package com.interview.PairEmployees.model;

import com.opencsv.bean.CsvBindByPosition;

public class Employee {

	@CsvBindByPosition(position = 0)
	private Integer empId;
	
	@CsvBindByPosition(position = 1)
	private int projectId;
	
	@CsvBindByPosition(position = 2)
	private String dateFrom;
	
	@CsvBindByPosition(position = 3)
	private String dateTo;
	
	private long daysWorkingOnProject;
	
	public Employee() {
		
	}

	public Employee(int empId, int projectId, String dateFrom, String dateTo) {
		this.empId = empId;
		this.projectId = projectId;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}
	
	public int getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	
	public long getDaysWorkingOnProject() {
		return daysWorkingOnProject;
	}

	public void setDaysWorkingOnProject(long daysWorkingOnProject) {
		this.daysWorkingOnProject = daysWorkingOnProject;
	}

	
	@Override
	public String toString() {
		return "Data [empId=" + empId + ", projectId=" + projectId + ", dateFrom=" + dateFrom + ", dateTo=" + dateTo
				+ "]";
	}
}
