package com.interview.PairEmployees.utilities;

import java.util.Date;

public interface CustomDateFormatter {
	
	public Date parseDate(String inputDate);
	
	public long getDateDif(String startDate, String endDate);

}