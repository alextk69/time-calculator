package com.interview.PairEmployees.utilities;

import java.time.Instant; 
import java.time.temporal.ChronoUnit;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;

@Service
public class CustomDateFormatterImpl implements CustomDateFormatter {
	
	private final String[] POSSIBLE_FORMATS = {
			"yyyy.MM.dd G 'at' HH:mm:ss z",
            "EEE, MMM d, ''yy",
            "h:mm a",
            "hh 'o''clock' a, zzzz",
            "K:mm a, z",
            "yyyyy.MMMMM.dd GGG hh:mm aaa",
            "EEE, d MMM yyyy HH:mm:ss Z",
            "yyMMddHHmmssZ",
            "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
            "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
            "YYYY-'W'ww-u",
            "EEE, dd MMM yyyy HH:mm:ss z", 
            "EEE, dd MMM yyyy HH:mm zzzz",
            "yyyy-MM-dd'T'HH:mm:ssZ",
            "yyyy-MM-dd'T'HH:mm:ss.SSSzzzz", 
            "yyyy-MM-dd'T'HH:mm:sszzzz",
            "yyyy-MM-dd'T'HH:mm:ss z",
            "yyyy-MM-dd'T'HH:mm:ssz", 
            "yyyy-MM-dd'T'HH:mm:ss",
            "yyyy-MM-dd'T'HHmmss.SSSz",
            "yyyy-MM-dd",
            "yyyyMMdd",
            "dd/MM/yy",
            "dd/MM/yyyy"
	};
	
	@Override
	public Date parseDate(String inputDate) {
		Date outputDate = null;
		try {
			outputDate = DateUtils.parseDate(inputDate, POSSIBLE_FORMATS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return outputDate;
	}
	
	@Override
	public long getDateDif(String startDate, String endDate) {
		
		Date date1 = this.parseDate(startDate);
		Date date2 = this.parseDate(endDate);
		
		Instant instant_date1 = date1.toInstant().truncatedTo(ChronoUnit.DAYS);
		Instant instant_date2 = date2.toInstant().truncatedTo(ChronoUnit.DAYS);
		
		long daysBetween = ChronoUnit.DAYS.between(instant_date1, instant_date2);
		
		return Math.abs(daysBetween);
	}
	
}
	