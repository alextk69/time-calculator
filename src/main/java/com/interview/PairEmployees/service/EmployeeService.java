package com.interview.PairEmployees.service;

import java.util.ArrayList;  
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.interview.PairEmployees.model.Employee;
import com.interview.PairEmployees.model.SortedEmployees;

@Service
public class EmployeeService {
	
	
	public List<SortedEmployees> getPairOfEmployeesForEveryProject(List<Employee> employeesList) {
		
		 Map<Integer, List<Employee>> employees = this.convertFromListToMapByProject(employeesList);

		 List<SortedEmployees> orderedEmployeesForProject = new ArrayList<>();
		 for(Entry<Integer, List<Employee>> entry : employees.entrySet()) {
			List<Employee> employeesForProject = entry.getValue();
			 employeesForProject.sort(Comparator.comparing(Employee::getDaysWorkingOnProject).reversed());
			 if (employeesForProject.size() < 2) {
				continue;
			}
			 SortedEmployees sortedEmployees = new SortedEmployees();
			 for (int i = 0; i < 2; i++) {
				 if (i == 0) {
					 sortedEmployees.setEmp1(employeesForProject.get(i).getEmpId());
					 sortedEmployees.setEmp1DaysWorkedOnProject(employeesForProject.get(i).getDaysWorkingOnProject());;
					 sortedEmployees.setProjectBothWorkedOn(employeesForProject.get(i).getProjectId());
					 continue;
				 }
				 if (i == 1) {
					 sortedEmployees.setEmp2(employeesForProject.get(i).getEmpId());
					 sortedEmployees.setEmp2DaysWorkedOnProject(employeesForProject.get(i).getDaysWorkingOnProject());;
					 sortedEmployees.setProjectBothWorkedOn(employeesForProject.get(i).getProjectId());
					 sortedEmployees.setDaysWorkedTogether(
							 (sortedEmployees.getEmp1DaysWorkedOnProject() < sortedEmployees.getEmp2DaysWorkedOnProject()) ? 
									 sortedEmployees.getEmp1DaysWorkedOnProject() : sortedEmployees.getEmp2DaysWorkedOnProject());
					 orderedEmployeesForProject.add(sortedEmployees);
				 }
			}
		}
		return orderedEmployeesForProject;
		
	}
	private Map<Integer, List<Employee>> convertFromListToMapByProject(List<Employee> employeesList) {
		return employeesList.stream().collect(Collectors.groupingBy(Employee::getProjectId));
	}

}