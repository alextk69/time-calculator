package com.interview.PairEmployees.controller;

import java.io.BufferedReader;  
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.interview.PairEmployees.model.Employee;
import com.interview.PairEmployees.model.SortedEmployees;
import com.interview.PairEmployees.service.EmployeeService;
import com.interview.PairEmployees.utilities.CustomDateFormatter;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

@Controller
public class UploadController {

	private CustomDateFormatter customDateFormatter;
	
	public UploadController(CustomDateFormatter theCustomDateFormatter) {
		customDateFormatter = theCustomDateFormatter;
	}
	
	@Autowired
	private EmployeeService service;
	
	@GetMapping("/")
	public String index() {
		return "index";
	}	
	
	@PostMapping("/upload-csv-file")
	public String uploadCsvFile(@RequestParam("file") MultipartFile file, Model theModel) {
		
		if (file.isEmpty()) {
			theModel.addAttribute("message", "Please choose a CSV file");
			theModel.addAttribute("status", false);
		} else {
			
			try(Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
				
				CsvToBean<Employee> csvToBean = new CsvToBeanBuilder<Employee>(reader)
						.withType(Employee.class)
						.withIgnoreLeadingWhiteSpace(true)
						.build();
				
				List<Employee> employeesList = csvToBean.parse();
				employeesList.stream().forEach(e -> e.setDaysWorkingOnProject(customDateFormatter.getDateDif(e.getDateFrom(), e.getDateTo())));
				List<SortedEmployees> topTwoLongestWorkingEmployeesPerProject = service.getPairOfEmployeesForEveryProject(employeesList);

				theModel.addAttribute("data", topTwoLongestWorkingEmployeesPerProject);
				theModel.addAttribute("status", true);
				
				
			} catch (Exception exc) {
				theModel.addAttribute("message", "Error processing CSV file...");
				theModel.addAttribute("status", false);
			}
			
		}
		return "upload-status";
	}
	
}
