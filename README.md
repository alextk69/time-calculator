# Time calculator

## Description

Calculator which groups all the people in a team in pairs who were working on a common project the longest period of time.

## Usage

The calculator itself has a pretty simple interface with a button to browse for a CSV file and another button to submit the file.
The results in which only one person participated will be removed from the list.
The final result only contains pair of employees who worked together on the project the longest, if for example there are 5 people working on a single project, only the two who worked the longest together will be shown and the rest will be excluded.
In the resource section of the app, there is a testFile.csv, that can be used to check the functionality.

The CSV file should have the following structure:
EmployeeID, ProjectID, StartDate, EndDate



## Project status
